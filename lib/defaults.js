"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const path_1 = tslib_1.__importDefault(require("path"));
const defaults = {
    pathProject: path_1.default.resolve(__dirname, '..'),
    customController: {},
    assetsFolder: '/assets/{param*}',
};
//# sourceMappingURL=defaults.js.map