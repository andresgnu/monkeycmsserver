export interface IOptionCmsServer {
    pathProject: string;
}
export declare const MonkeyUI: {
    name: string;
    version: string;
    register: (server: any, options: IOptionCmsServer) => Promise<void>;
};
