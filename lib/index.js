"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cms_monkey_lib_1 = require("cms-monkey-lib");
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
exports.MonkeyUI = {
    name: 'monkey-cms-server',
    version: '0.1.1',
    register: (server, options) => __awaiter(this, void 0, void 0, function* () {
        // Create a route for example
        const pathProject = options.pathProject;
        server.route([
            {
                method: 'POST',
                path: '/site-map',
                handler: (req, h) => __awaiter(this, void 0, void 0, function* () {
                    const Routes = new cms_monkey_lib_1.ReadPages(pathProject, 'data/routes');
                    const Blogs = new cms_monkey_lib_1.ReadPages(pathProject, 'data/blogs', {
                        root: 'blogs',
                    });
                    let AllPages = [];
                    AllPages = AllPages.concat(Routes.readPagesList, Blogs.readPagesList);
                    let AllPagesTree = [];
                    AllPagesTree = AllPagesTree.concat(Routes.readPagesTree, [
                        { id: 'blogs', children: Blogs.readPagesTree },
                    ]);
                    return h.response({ tree: AllPagesTree, all: AllPages });
                }),
            },
            {
                method: 'PUT',
                path: '/site-map/page',
                handler: (req, h) => __awaiter(this, void 0, void 0, function* () {
                    const page = req.payload;
                    const p = path_1.default.join(pathProject, page.path);
                    delete page.path;
                    delete page.fullUrl;
                    delete page.id;
                    fs_1.default.writeFileSync(p, JSON.stringify(page), 'utf8');
                    return h.response('Bien');
                }),
            },
        ]);
    }),
};
//# sourceMappingURL=index.js.map