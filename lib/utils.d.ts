import { Request, ResponseToolkit } from 'hapi';
export declare type THapiCustom = (req: Request, h: ResponseToolkit) => any;
export declare const BuildPage: (AllCustom: {
    [key: string]: THapiCustom;
}) => (req: Request, h: ResponseToolkit) => Promise<any>;
declare type PAGE = (url: string) => any;
declare type ReadImages = (pathFiles: string, pathUrl: string) => any;
export declare const PreBuildPage: (Pages: PAGE, IMAGES: ReadImages, paths: {
    files: string;
    urls: string;
}) => ({
    method: (request: Request, h: ResponseToolkit) => any;
    assign: string;
}[] | {
    method: (request: Request, h: ResponseToolkit) => any;
    assign: string;
})[];
export {};
