"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Console = console;
exports.BuildPage = (AllCustom) => (req, h) => tslib_1.__awaiter(this, void 0, void 0, function* () {
    Console.log('********Ir a **********', req.params.name);
    Console.log('Params', req.query.prod);
    const page = Object.assign({}, req.pre.page, { root: '/', PROD: req.query.prod });
    if (page.PAGE) {
        if (page.PAGE.custom) {
            if (AllCustom[page.PAGE.custom]) {
                const method = AllCustom[page.PAGE.custom](req, h);
                return yield method(page);
            }
            else {
                return h.redirect('/404.html');
            }
        }
        return h.view(page.PAGE.view, Object.assign({}, page));
    }
    else {
        return h.redirect('/404.html');
    }
});
exports.PreBuildPage = (Pages, IMAGES, paths) => {
    return [
        [
            {
                method: (request, h) => {
                    try {
                        if (request.url.pathname === '/') {
                            return Pages('index.html');
                        }
                        else {
                            const p = Pages(request.params.name);
                            if (p) {
                                return p;
                            }
                            else {
                                return {};
                            }
                        }
                    }
                    catch (error) {
                        Console.log(error);
                        return {};
                    }
                }, assign: 'context',
            }, {
                method: (request, h) => {
                    try {
                        return IMAGES(paths.files, paths.urls);
                    }
                    catch (error) {
                        return {};
                    }
                }, assign: 'images',
            },
        ],
        {
            method: (request, h) => {
                return Object.assign({ IMAGES: request.pre.images }, request.pre.context);
            }, assign: 'page',
        },
    ];
};
//# sourceMappingURL=utils.js.map