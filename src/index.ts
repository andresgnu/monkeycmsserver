import { Request, ResponseToolkit } from 'hapi';
import { IPage, ReadPages } from 'cms-monkey-lib';
import fs from 'fs';
import path from 'path';

export interface IOptionCmsServer {
    pathProject: string;
}
export const MonkeyUI = {
    name: 'monkey-cms-server',
    version: '0.1.1',
    register: async (server: any, options: IOptionCmsServer) => {
        // Create a route for example
        const pathProject = options.pathProject;

        server.route([
            {
                method: 'POST',
                path: '/site-map',
                handler: async (req: Request, h: ResponseToolkit) => {
                    const Routes = new ReadPages(pathProject, 'data/routes');
                    const Blogs = new ReadPages(pathProject, 'data/blogs', {
                        root: 'blogs',
                    });
                    let AllPages: IPage[] = [];
                    AllPages = AllPages.concat(
                        Routes.readPagesList,
                        Blogs.readPagesList,
                    );
                    let AllPagesTree: any[] = [];
                    AllPagesTree = AllPagesTree.concat(Routes.readPagesTree, [
                        { id: 'blogs', children: Blogs.readPagesTree },
                    ]);

                    return h.response({ tree: AllPagesTree, all: AllPages });
                },
            },
            {
                method: 'PUT',
                path: '/site-map/page',
                handler: async (req: Request, h: ResponseToolkit) => {
                    const page: any = req.payload;
                    const p = path.join(pathProject, page.path);

                    delete page.path;
                    delete page.fullUrl;
                    delete page.id;

                    fs.writeFileSync(p, JSON.stringify(page), 'utf8');
                    return h.response('Bien');
                },
            },
        ]);
    },
};
